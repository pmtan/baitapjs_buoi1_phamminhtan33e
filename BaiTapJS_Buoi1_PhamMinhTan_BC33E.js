/*
 Bài 1: Tính Lương Nhân Viên
- Đầu vào: Só ngày làm

- Xử lý: Lương =  Lương 1 ngày x Số ngày làm

- Đầu ra: Lương nhân viên
*/

var luongNgay = 100000;

var soNgayLam = 26;

var luongNhanVien = luongNgay * soNgayLam;
console.log("Bai 1: Tinh Luong Nhan Vien");
console.log("Luong Ngay: ", luongNgay);
console.log("So Ngay Lam: ", soNgayLam);
console.log("Luong Nhan Vien: ", luongNhanVien);


/*
Bài 2: Tính Giá Trị Trung Bình
- Đầu vào: 5 số thực num1, num2, num3, num4, num5

- Xử lý: trungBinh =  (num1 + num2 + num3 + num4 + num5)/5

- Đầu ra: Giá trị trung bình của 5 số

*/

var num1 = 1;
var num2 = 2;
var num3 = 3;
var num4 = 4;
var num5 = 5;

var average = (num1 + num2 + num3 + num4 +num5)/ 5;
console.log("Bai 2: Tinh Gia Tri Trung Binh");
console.log("Gia Tri:", num1, num2, num3, num4, num5);
console.log("Gia Tri Trung Binh: ", average);

/*
Bài 3: Quy Đổi Tiền
- Đầu vào: Số USD cần quy đổi

- Xử lý: Số tiền VNĐ =  Số USD cần quy đổi x Giá quy đổi hiện nay

- Đầu ra: Số tiền VNĐ được quy đổi từ số USD
*/

var soUsd = 20;
const tiGia = 23500;
var soVnd = soUsd * tiGia;
console.log("Bai 3: Quy Doi Tien");
console.log("USD: ", soUsd);
console.log("Ti Gia: ", tiGia);
console.log("VND: ", soVnd);


/*
Bài 4: Tính Chu Vi, Diện Tích Hình Chữ Nhật
- Đầu vào: chiều dài , chiều rộng

- Xử lý:    chuVi = (chiều dài + chiều rộng) x 2
            dienTich = chiều dài x chiều rộng

- Đầu ra: chu vi, diện tích của hình chữ nhật
*/

var chieuDai = 10;
var chieuRong = 5;

var chuVi = (chieuDai + chieuRong)*2;
var dienTich = chieuDai * chieuRong;
console.log("Bai 4: Tinh Chu Vi, Dien Tich Hinh Chu Nhat");
console.log("Chieu dai: ", chieuDai);
console.log("Chieu rong: ", chieuRong);
console.log("Chu Vi: ", chuVi);
console.log("Dien Tich: ", dienTich);

/*
Bài 5: Tính Tổng 2 Ký Số
- Đầu vào: 1 số có 2 chữ số: num

- Xử lý:    Lấy số hàng đơn vị = int(num % 10)
            Lấy số hàng chục = int(num/ 10)
            Tổng ký số =  số hàng chục + số hàng đơn vị

- Đầu ra: Tổng ký số
 */
var number = 99;

var hangDonVi = (number % 10);
var hangChuc = Math.floor(number / 10);
var tongKySo = hangChuc + hangDonVi;

console.log("Bai 5: Tinh Tong 2 Ky So");
console.log("So Da Nhap: ", number);
console.log("Tong Ky So: ", tongKySo);
